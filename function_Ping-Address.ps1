<#
.SYNOPSIS
Pings an IP address, checks ARP cache, and looks for DN PTR records

.EXAMPLE
C:\> 2..50 | Foreach-Object { "10.0.0.$_" } | Ping-Address

.EXAMPLE
C:\> $scan = 2..50 | Foreach-Object { "10.0.0.$_" } | Ping-Address
C:\> $scan | Out-GridView
C:\> $scan | Export-Csv scan-102.csv -NoTypeInfo

.EXAMPLE
C:\> 2..50 | Foreach-Object { "10.0.0.$_" } | Ping-Address | Export-Csv scan-102.csv -NoTypeInfo
#>
function Ping-Address {
    param(
        # IP Address to check
        [Parameter(Mandatory,ValueFromPipeline)]
        [string[]]
        $IPAddress
    )

    begin{
        function Get-PingStatus {
            param(
                [Parameter(Mandatory)]
                [uint32]$StatusCode 
            )
            
            Switch ($StatusCode) {
                0     {'Success'}
                11002 {'Destination Network Unreachable'}
                11003 {'Destination Host Unreachable'}
                11010 {'Request Timed Out'}
                default {"Other Failure ($StatusCode)"}  
            }
        }
    }

    PROCESS {
        foreach ($IP in $IPAddress) {
            Write-Progress -Activity 'Pinging adddress' -Status "Pinging $IP"
            $ping = ''
            #$query  = "SELECT * FROM Win32_PingStatus WHERE Address = '$IP'" #AND ResolveAddressNames = $true"
            $wmi_ping = Get-WmiObject Win32_PingStatus -filter "Address='$IP'"

            #foreach ($result in $results) {
            if ($wmi_ping.StatusCode -eq 0) {
                $ping = 'ping!'
                Write-Progress -Activity 'Pinging adddress' -Status "Pinging $IP - Success"
            }
            else {
                Write-Progress -Activity 'Pinging adddress' -Status "Pinging $IP - No response"
            }
            #}
 
            $dnsnames = (Resolve-DnsName -Name $IP -EA 'SilentlyContinue' | Select -Expand NameHost) -join ','
            $macaddr  = ( ( arp.exe -a | Select-String -Pattern "\b$IP\b" ).line -split '\s+' )[2]

            [PSCustomObject]@{
                IPAddress  = $IP
                Ping       = $ping
                StatusCode = $wmi_ping.StatusCode
                Status     = (Get-PingStatus $wmi_ping.StatusCode)
                MACAddr    = $macaddr
                DNSName    = $dnsnames
            }


        }
   }
   end{}
}
